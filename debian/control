Source: libcrypt-util-perl
Section: perl
Priority: optional
Build-Depends: cdbs,
 devscripts,
 perl,
 debhelper (>= 10~),
 dh-buildinfo,
 libdata-guid-perl,
 libmoose-perl,
 libsub-exporter-perl,
 libnamespace-clean-perl,
 libtest-exception-perl,
 libtest-simple-perl (>= 1.001010) | libtest-use-ok-perl | perl (>= 5.21.6),
 libcrypt-cbc-perl,
 libcrypt-rijndael-perl,
 libdigest-hmac-perl,
 libmime-base32-perl,
 libmime-base64-urlsafe-perl,
 liburi-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.4
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcrypt-util-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcrypt-util-perl
Homepage: https://metacpan.org/release/Crypt-Util
Testsuite: autopkgtest-pkg-perl

Package: libcrypt-util-perl
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${perl:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Description: lightweight Crypt/Digest convenience API
 Crypt::Util provides an easy, intuitive and forgiving API for wielding
 crypto-fu.
 .
 The API is designed as a cascade, with rich features built using
 simpler ones. This means that the option processing is uniform
 throughout, and the behaviors are generally predictable.
 .
 Note that Crypt::Util doesn't do any crypto on its own, but delegates
 the actual work to the various other crypto modules on the CPAN.
 Crypt::Util merely wraps these modules, providing uniform parameters,
 and building on top of their polymorphism with higher level features.
